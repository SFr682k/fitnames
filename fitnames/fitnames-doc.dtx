%\iffalse
%%  Doc-Source file to use with LaTeX2e
%%  Copyright (C) 2018 Sebastian Friedl
%%
%%  This work is subject to the LaTeX Project Public License, Version 1.3c or -- at
%%  your option -- any later version of this license.
%%  The work consists of the files fitnames.sty and fitnames-doc.dtx
%%
%%  This work has the LPPL maintenance status ‘maintained’.
%%  Current maintainer of the work is Sebastian Friedl.
%%
%%  -------------------------------------------------------------------------------------------
%%
%%  Fit and truncate names so that they do not occupy more than a given width.
%%
%%  -------------------------------------------------------------------------------------------
%%
%%  Please report bugs and other problems as well as suggestions for improvements
%%  to my email address (sfr682k@t-online.de).
%%
%%  -------------------------------------------------------------------------------------------
%\fi

% !TeX spellcheck=en_US

\documentclass[11pt]{ltxdoc}

\usepackage{iftex}
\RequireLuaTeX

\usepackage[no-math]{fontspec}
\usepackage{polyglossia}
\setdefaultlanguage{english}
\usepackage[english]{selnolig}

\usepackage{array}
\usepackage{csquotes}
\usepackage{hologo}
\usepackage[unicode, pdfborder={0 0 0}, linktoc=all, hyperindex=false]{hyperref}
\usepackage{multicol}
\usepackage{tikz}
\usepackage{xcolor}


\usepackage{fitnames}


\parindent0pt

\usepackage[erewhon]{newtxmath}
\setmainfont{erewhon}
\setsansfont[Scale=MatchLowercase]{Source Sans Pro}
\setmonofont[Scale=MatchLowercase]{Hack}

\usepackage[a4paper,left=4.50cm,right=2.75cm,top=3.25cm,bottom=2.75cm,nohead]{geometry}


\hyphenation{re-sul-ting floa-ting sca-ling}

\def\Lua{\textsf{Lua}\ignorespaces}

\usetikzlibrary{arrows.meta,positioning,calc,shapes}
\tikzset{>={Stealth[scale=1.25]}}
\tikzstyle{switchnode}  = [draw, diamond, align=center, aspect=3]
\tikzstyle{cmdnode}     = [draw, inner xsep=1em, inner ysep=1ex, text width=3.25cm, align=center]
\tikzstyle{truearrow}   = [green!50!black, ->]
\tikzstyle{falsearrow}  = [red, ->]
\tikzstyle{startsignal} = [text width=.75em, minimum height=5ex, align=center, signal, signal from=nowhere, signal to=north]
\tikzstyle{contsignal}  = [text width=.75em, minimum height=5ex, align=center, signal, signal from=south,   signal to=north]
\tikzstyle{endsignal}   = [text width=.75em, minimum height=5ex, align=center, signal, signal from=south,   signal to=nowhere]
\tikzstyle{descrnode}   = [inner xsep=0pt, inner ysep=.5ex, text width=10.5cm, anchor=west, align=left]


\MakeShortVerb{"}
\CheckSum{336}

\renewcommand*{\usage}[1]{\hyperpage{#1}}
\renewcommand*{\main}[1]{\hyperpage{#1}}

\newcommand*{\opt}[1]{\texttt{#1}\index{#1=\texttt{#1}|main}}
\newcommand*{\sty}[1]{\textsf{#1}}
\newcommand*{\lib}[1]{\textsf{#1}}


\newcommand{\DescribeOption}[4]{%
    \index{#1=\texttt{#1}~~option|main}%
    \leavevmode%
    \marginpar{\raggedleft\PrintDescribeMacro{#1}}%
    \begin{minipage}[t]{\textwidth}
        \textit{\textbf{#2}}\dotfill~#3\par
        \begingroup
        \vspace{0.5em}#4\par
        \endgroup
    \end{minipage}%
    \bigskip\medskip}

\def\param#1{\textit{\rmfamily\ensuremath{\langle}#1\ensuremath{\rangle}}}
    

\PageIndex
\RecordChanges


\newlength{\codeindent}
\settowidth{\codeindent}{\texttt{~~~~}}
\newlength{\codeiindent}
\settowidth{\codeiindent}{\texttt{~~~~~\,}}
\newlength{\codeiiindent}
\settowidth{\codeiiindent}{\texttt{~~~~~~~\,}}

\def\fitnamesexample#1{{→|\underline{\hbox to #1 {\hfill\fitname{Müller-Lüdenscheid}{Paul-Otto Norbert}{#1}\hfill}}|←}}




\title{The \sty{fitnames} package \\ {\large\url{https://gitlab.com/SFr682k/fitnames}}}
\author{Sebastian Friedl \\ \href{mailto:sfr682k@t-online.de}{\ttfamily sfr682k@t-online.de}}
\date{2018/07/10}

\hypersetup{pdftitle={The fitnames package},pdfauthor={Sebastian Friedl}}

\begin{document}
    \maketitle
    \thispagestyle{empty}
    
    
%     \begin{center} \itshape
%         \enquote{} \\
%         --- \textsc{\upshape } ---
%     \end{center}
    
    \medskip
    \begin{abstract}
        \noindent\centering%
        Fit and truncate names so that they do not occupy more than a given width.
    \end{abstract}
    
    
    
    \vfill
    \subsection*{License}
    \addcontentsline{toc}{subsection}{License}
    \textcopyright\ 2018 Sebastian Friedl
    
    \smallskip
    This work may be distributed and/or modified under the conditions of the \LaTeX\ Project Public License, either version 1.3c of this license or (at your option) any later version.
    
    \smallskip
    The latest version of this license is available at \url{http://www.latex-project.org/lppl.txt} and version 1.3c or later is part of all distributions of \LaTeX\ version 2008-05-04 or later.
    
    \smallskip
    This work has the LPPL maintenace status \enquote*{maintained}. \\
    Current maintainer of this work is Sebastian Friedl.
    
    \medskip
    This work consists of the following files:
    \begin{multicols}{2}
        \begin{itemize} \itemsep 0pt
            \item "fitnames.dtx",
            \item "fitnames.ins",
            \item "fitnames-doc.dtx" and
            \item the derived file "fitnames.sty"
        \end{itemize}
    \end{multicols}
    
    
    
    \clearpage
    
    \phantomsection
    \addcontentsline{toc}{subsection}{Contents}
    \tableofcontents
    
    \clearpage
    
    
    
    \subsection*{Dependencies and other requirements}
    \addcontentsline{toc}{subsection}{Dependencies and other requirements}
    \sty{fitnames} depends on the following packages:
    \begin{multicols}{4}\sffamily\centering
        calc \\ graphicx \\ if\kern0pt tex \\ ifthen \\ luacode \\ lualibs \\ pgfkeys \\ pgfopts
    \end{multicols}

    
    Furthermore, since \sty{fitnames} internally uses some \Lua\ code, documents using \sty{fitnames} have to be compiled using \hologo{LuaLaTeX}.
    
    
    
    \subsection*{Installation}
    \addcontentsline{toc}{subsection}{Installation}
    Extract the \textit{package} file first:
    \begin{enumerate}
        \item Run \LaTeX\ over the file "fitnames.ins"
        \item Move the resulting ".sty" file to "TEXMF/tex/lualatex/fitnames/"
    \end{enumerate}
    
    Then, you can compile the \textit{documentation} yourself by executing \\[\smallskipamount]
    "lualatex fitnames-doc.dtx" \\
    "makeindex -s gind.ist fitnames-doc.idx" \\
    "makeindex -s gglo.ist -o fitnames-doc.gls fitnames-doc.glo" \\
    "lualatex fitnames-doc.dtx" \\
    "lualatex fitnames-doc.dtx"
    
    \smallskip
    or just use the precompiled documentation shipped with the source files. \\
    In both cases, copy the files "fitnames-doc.pdf" and "README.md" to \\
    "TEXMF/doc/lualatex/fitnames/"
    
    
    





    % DOCUMENTATION PART ----------------------------------------------------------------------
    \clearpage
    \part{The documentation}
    
    \begin{figure}[h]\centering
        \def\arraystretch{1.15}
        \begin{tabular}{>{\itshape\small\sffamily}r>{\large\sffamily}c}
            6.6 cm & \fitnamesexample{6.6cm} \\
            6.5 cm & \fitnamesexample{6.5cm} \\
            6.4 cm & \fitnamesexample{6.4cm} \\
            6.3 cm & \fitnamesexample{6.3cm} \\
            6.2 cm & \fitnamesexample{6.2cm} \\
            6.1 cm & \fitnamesexample{6.1cm} \\
            6.0 cm & \fitnamesexample{6.0cm} \\
            5.9 cm & \fitnamesexample{5.9cm} \\
            5.8 cm & \fitnamesexample{5.8cm} \\
            5.7 cm & \fitnamesexample{5.7cm} \\
            5.6 cm & \fitnamesexample{5.6cm} \\
            5.5 cm & \fitnamesexample{5.5cm} \\
            5.4 cm & \fitnamesexample{5.4cm} \\
            5.2 cm & \fitnamesexample{5.2cm} \\
            5.0 cm & \fitnamesexample{5.0cm} \\
            4.8 cm & \fitnamesexample{4.8cm} \\
            4.6 cm & \fitnamesexample{4.6cm} \\
            4.4 cm & \fitnamesexample{4.4cm} \\
            4.2 cm & \fitnamesexample{4.2cm} \\
            4.0 cm & \fitnamesexample{4.0cm} \\
            3.6 cm & \fitnamesexample{3.6cm} \\
            3.2 cm & \fitnamesexample{3.2cm} \\
            2.8 cm & \fitnamesexample{2.8cm} \\
            2.4 cm & \fitnamesexample{2.4cm} \\
            2.0 cm & \fitnamesexample{2.0cm} \\
            1.6 cm & \fitnamesexample{1.6cm} \\
            1.2 cm & \fitnamesexample{1.2cm} \\
            0.8 cm & \fitnamesexample{0.8cm}
        \end{tabular}
    
        \caption{A very long (fictional) name fitted into different widths}
    \end{figure}

    
    
    
    \clearpage
    \section{General notes}
    \subsection{Purpose of application}\label{sec:applications}
    The \sty{fitnames} package provides a solution for different scenarios where a name should not occupy more than a given width of the current line.
    
    \smallskip
    Originally designed for squeezing and -- if required -- truncating names to fit them on a name plate, the package has been extended by a configuration interface to be adaptable for other applications in non-justified paragraphs, e.g. preparing registers.
    
    \medskip
    Please note that \sty{fitnames} has \emph{never} been intended for use in running text. The algorithms used for implementing its functionality heavily depend on "save"- and "resizeboxes", so you should expect unaesthetic typographical results when using the "\fitname" command in a normal, justified paragraph. This limitation is considered as \enquote{by design}, so there will be no \enquote{patches} removing it.
    
    
    \subsection{Known limitations}
    \begin{itemize}
        \item
            Since names processed by the "\fitname" command are put into a single box \dots
            \begin{itemize}\topsep0pt
                \item[\dots] names are not wrapped over lines and
                \item[\dots] spaces inside the names are not stretched inside justified lines.
            \end{itemize}
            This behavior is \emph{not} a bug, but a side-effect of the implementation required for being able to scale names. Since \sty{fitnames} is \emph{not} designed for use in running text (see section~\ref{sec:applications}), there will be \emph{no patches removing these side effects}.
        
        \item
            The truncation algorithms' rules are based on a quite rigid set of assumptions. \\
            While the rules applied for truncating last names may be influenced by defining replacement rules (see section~\ref{sec:reprules}), a similar mechanism for influencing the truncation of first names is \emph{not} provided.
            
        \item
            Even \sty{fitnames} won't succeed in fitting very long names into few space. \\
            Therefore, if a name is not adjustable into the given width, it will occupy more than the specified width and a warning is printed to the log.
    \end{itemize}
    
    
    
    
    
    \clearpage
    \section{Basic usage}
    \subsection{Loading the package}
    Loading \sty{fitnames} is as easy as adding "\usepackage{fitnames}" to your document's preamble. However, since parts of \sty{fitnames} are written in \Lua, your document \emph{has} to be compiled using \hologo{LuaLaTeX} now.
    
    \bigskip
    \sty{fitnames}' default configuration uses sensible values which should be suitable for common use. However, if you are not satisfied with the results delivered by them, a sophisticated configuration interface is provided (see section~\ref{sec:config}).
    
    
    \subsection{Fitting names}
    The "\fitname" command is provided for fitting names into a custom width. Its syntax is:
    %
    \begin{center}\DescribeMacro{\fitname}
        "\fitname{"\param{last name}"}{"\param{first name}"}{"\param{width}"}"
    \end{center}
    %
    The required arguments are …
    \begin{itemize}\itemsep0pt
        \item[\dots] the last name \param{last name},
        \item[\dots] the first name \param{first name} and
        \item[\dots] the width \param{width} the name is allowed to occupy.
    \end{itemize}

    For example, "\fitname{Doe}{John}{1.2cm}" will fit the name \textit{John Doe} into a width of up to 1.2\,cm. The result might be something like \enquote{\fitname{Doe}{John}{1.2cm}}.
    
    
    
    
    
    \clearpage
    \section{Advanced usage}\label{sec:config}
    \subsection{Configuring \sty{fitnames}}
    \subsubsection{Early and late selection of options}
    \sty{fitnames} provides a set of configuration keys (see section~\ref{sec:config-keys}).
    
    These configuration keys can be used
    \begin{itemize}\itemsep0pt
        \item as package options \\
            \textit{Example:} "\usepackage[lastnamefirst=true, lastnamefont=sc]{fitnames}"
        
        \item\DescribeMacro{\fitnamesconfig} as arguments of the "\fitnamesconfig" command \\
            \textit{Example:} "\fitnamesconfig{lastnamefirst=true, lastnamefont=sc}"
    \end{itemize}
    
    
    \subsubsection{Available configuration keys}\label{sec:config-keys}
    The list of available configuration keys is structured as shown in the following example:
    
    \medskip\leavevmode%
    \marginpar{\raggedleft\PrintDescribeMacro{key}}%
    \begin{minipage}[t]{\textwidth}
        \textit{\textbf{possible values if the key requires a choice}}\dotfill~default value\par
        \begingroup
        \vspace{0.5em}Description of both, the option and the values' effects\par
        \endgroup
    \end{minipage}%
    \medskip\smallskip
    
    \begin{center}
        ------------~~·~~------------
    \end{center}
    
    
    \DescribeOption{lastnamefirst}{true, false}{false}{%
        Determines the order of first and last name.
        \begin{description}\itemsep0pt
            \item[true]
                Print the last name before the first name (e.g.~\enquote{Doe, John})
                
            \item[false]
                Print the first name before the last name (e.g.~\enquote{John Doe})
        \end{description}}
    
    \DescribeOption{firstname}{full, truncate, firstletter, omit}{full}{%
        Determines how the first name is pre-processed, i.e. displayed without any fitting-related truncation:
        \begin{description}\itemsep0pt
            \item[full]
                Don't pre-process the first name
            
            \item[truncate]
                Softly truncate the first name, i.e. pre-process \enquote{Rolf-Soeren Walter} to \enquote{Rolf-S.}
            
            \item[firstletter]
                Truncate the first name to a single letter, i.e. pre-process \enquote{Rolf-Soeren Walter} to \enquote{R.}
            
            \item[omit]
                Omit the first name completely, i.e. print only the last name
        \end{description}}
    
    \DescribeOption{firstnamefont}{bf, it, bf{\kern0pt}it, sc, text}{text}{%
        Determines the font switches invoked for printing the first name. \\
        This does \emph{not} override the font used in the current paragraph.
        \begin{description}\itemsep0pt
            \item[bf] Add the \texttt{\textbackslash bfseries} switch
            \item[it] Add the \texttt{\textbackslash itshape} switch
            \item[bf{\kern0pt}it] Add the "\texttt{\textbackslash bfseries\textbackslash itshape}" switches
            \item[sc] Add the \texttt{\textbackslash scshape} switch
            \item[text] Do not add any switches, i.e. use the current paragraph's font
        \end{description}}
    
    \DescribeOption{lastname}{full, truncate}{full}{%
        Determines how the last name is pre-processed, i.e. displayed without any fitting-related truncation:
        \begin{description}\itemsep0pt
            \item[full]
                Don't pre-process the last name
            
            \item[truncate]
                Softly truncate the last name, i.e. pre-process \enquote{von Irgend-Wo} to \enquote{Irgend-W.}
        \end{description}}
    
    \DescribeOption{lastnamefont}{bf, it, bf{\kern0pt}it, sc, text}{text}{%
        Determines the font switches invoked for printing the last name. \\
        This does \emph{not} override the font used in the current paragraph.
        \begin{description}\itemsep0pt
            \item[bf] Add the \texttt{\textbackslash bfseries} switch
            \item[it] Add the \texttt{\textbackslash itshape} switch
            \item[bf{\kern0pt}it] Add the "\texttt{\textbackslash bfseries\textbackslash itshape}" switches
            \item[sc] Add the \texttt{\textbackslash scshape} switch
            \item[text] Do not add any switches, i.e. use the current paragraph's font
        \end{description}}
    
    \DescribeOption{namesep}{}{\texttt{\textbackslash space}~~~or~~~\texttt{,\textbackslash space}}{%
        The character sequence used to separate first and last name. \\
        By default, the name separator is\dots
        \begin{itemize}\itemsep0pt\topsep0pt
            \item a single space (e.g.~\enquote{John Doe}) if the first name is printed before the last name and
            \item a comma followed by a single space if the last name is printed before the first name (e.g.~\enquote{Doe, John}).
        \end{itemize}
        
        \smallskip
        \textit{Example usage:}\quad \texttt{namesep=\{,\textbackslash space\textbackslash space\}} \\
        Note that the separator key's value has to be enclosed into braces since the comma character has a special meaning when changing key's values.
        
        \medskip
        \textit{Note:} \\
        When changing the "lastnamefirst" key's value, the name separator gets adjusted to its respective default value as well.
    }
    
    \DescribeOption{minscale}{}{0.85}{%
        Determines the scale factor to which the name may be squeezed \emph{before truncating} it. \\
        The key's value may be any decimal number larger than zero. 
        
        \smallskip
        Setting the value to "1.0" or any larger number will disable scaling as long as further truncation of the name \emph{is} possible.
        
        \medskip
        \textit{Example usage:}\quad "minscale=0.825"
    }

    \DescribeOption{minradicalscale}{}{0.75}{%
        Determines the scale factor to which the name may be squeezed \emph{when further truncation is not possible}. \\
        The key's value may be any decimal number larger than zero.
        
        \smallskip
        Setting the value to "1.0" or any larger number will disable scaling when further truncation of the name is \emph{not} possible.
        
        \medskip
        \textit{Example usage:}\quad "minscale=0.70"
    }
    
    \vspace{-\bigskipamount}
    
    
    
    \subsubsection{Examples}\label{sec:config-examples}
    % auxiliary command
    \def\confexamplecmd{\textsf{\fitname{de Foo-Bar}{Marc-Louis John}{5cm}}}
    
    \begingroup % avoid that configuration changes in this section apply to the whole document
    
    The following command is used to typeset these examples:
    \begin{center}
        \vspace{-\smallskipamount}%
        "\fitname{de Foo-Bar}{Marc-Louis John}{5cm}"
    \end{center}
    
    Configuration changes apply until the end of this example; i.e. configuration changes made in step 1 will also affect steps 2ff.
    
    \medskip
    \begin{enumerate}
        \item[0.]
            Output without any configuration:
            \hfill\confexamplecmd
    
        \item
            "\fitnamesconfig{lastnamefirst=true}" \\
            \rightarrow\ the last name is printed before the first name:
            \fitnamesconfig{lastnamefirst=true}\hfill\confexamplecmd
        
        \item
            "\fitnamesconfig{lastnamefont=sc}" \\
            \rightarrow\ the last name is printed using \textsc{small caps}:
            \fitnamesconfig{lastnamefont=sc}\hfill\confexamplecmd
            
        \item
            "\fitnamesconfig{firstname=truncate}" \\
            \rightarrow\ softly truncate the first name before fitting:
            \fitnamesconfig{firstname=truncate}\hfill\confexamplecmd
    \end{enumerate}
    
    \bigskip
    The maximum allowed width gets reduced to 2.6\,cm. \\
    Squeezing is required to fit the name into this width:
    \def\confexamplecmd{\textsf{\fitname{de Foo-Bar}{Marc-Louis John}{2.6cm}}}
    \fitnamesconfig{lastnamefirst, lastnamefont=sc, firstname=truncate}\hfill\confexamplecmd

    \begin{enumerate}
        \item[4.]
            "\fitnamesconfig{minscale=1.0}" \\
            \rightarrow\ squeezing gets disabled if further truncation is possible:
            \fitnamesconfig{lastnamefirst, lastnamefont=sc, firstname=truncate, minscale=1.0}\hfill\confexamplecmd
    \end{enumerate}
    
    
    \endgroup
    
    
    
    \subsection{Using replacement rules}\label{sec:reprules}
    When truncating last names consisting of several parts separated by a space, \sty{fitnames} will omit everything but the last part; e.g. truncation of the last name \enquote{von der Waldlichtung} will result in \enquote{Waldlichtung}.
    
    \smallskip
    But there are cases where the omitted parts are essential. The provided \textit{replacement rules} do not only prevent the truncation algorithms from omitting the respective parts, they also define how these parts should be displayed after truncating. 
    

    \subsubsection{Defining replacement rules}
    Replacement rules are defined via the "\fitnamesreplace" command:
    \begin{center}\DescribeMacro{\fitnamesreplace}
        "\fitnamesreplace{"\param{appearance}"}{"\param{representation}"}"
    \end{center}
    %
    The required arguments are …
    \begin{itemize}\itemsep0pt
        \item[\dots] the \param{appearance} of this part in the non-truncated name and
        \item[\dots] the \param{representation} assigned to this part in the truncated name.
    \end{itemize}
    
    \medskip
    \textit{Runtime considerations:} \\
    The replacement rules are stored in a two-dimensional table. If a space-separated part is found inside the last name, \sty{fitnames} will scroll through the whole table until a match is found or all entries have been checked. \\
    Generally, the truncation algorithm is quite performant since it's completely written in \Lua, but for \emph{very} huge documents using "\fitname" \emph{many} times, compilation time will be saved if you \dots
    \begin{itemize}\itemsep0pt
        \item keep the number of replacement rules \emph{as small as possible} and
        \item declare more common used replacement rules before declaring less common ones.
    \end{itemize}

    
    
    \subsubsection{Deleting replacement rules}
    \emph{Individual} replacement rules are deleted via the "\fitnamesnoreplace" command:
    \begin{center}\DescribeMacro{\fitnamesnoreplace}
        "\fitnamesnoreplace{"\param{appearance}"}"
    \end{center}
    %
    Using this command deletes the replacement rule associated to parts of the last name appearing as \param{appearance}.
    
    \bigskip
    \DescribeMacro{\fitnamesclearreplacements}
    For deleting \emph{all} replacement rules, the "\fitnamesclearreplacements" command is provided. It doesn't require arguments.
    
    
    \subsubsection{Examples}
    
    
    
    
    
    \clearpage
    \section{Backstage: \sty{fitnames}' algorithm visualized}\label{sec:visuals}
    Figure~\ref{fig:alg-fittomaxwidth} depicts the recursive algorithm used for fitting names into maximum allowed width.
    Figure~\ref{fig:alg-truncation} shows the different levels of truncation applied to a fictional name.
    
    \begin{figure}[h!t]\centering
        \begin{tikzpicture}\sffamily\footnotesize
            \node[cmdnode] (compose) at (0,0)
                {Compose name from the passed arguments};
            
            \node[switchnode, below=4ex of compose] (compBoxSize)
                {Width of non-scaled name \\ larger than allowed width?};
            \draw[->] (compose) -- (compBoxSize);
            
            \node[cmdnode, below right=4ex of compBoxSize] (printBox)
                {Print the name without squeezing};
            \draw[falsearrow] (compBoxSize) -| node[above left] {false} (printBox);
            
            \node[cmdnode, below left=4ex of compBoxSize] (calcScalingFact)
                {Calculate the scaling factor required to fit the name into allowed width};
            \draw[truearrow] (compBoxSize) -| node[above right] {true} (calcScalingFact);
            
            \node[switchnode, below=4ex of calcScalingFact] (compScalingFact)
                {Scaling factor smaller \\ than min. scaling factor?};
            \draw[->] (calcScalingFact) -- (compScalingFact);
            
            \node[cmdnode, right=of compScalingFact] (printSqueezedName)
                {Print the name squeezed to allowed width};
            \draw[falsearrow] (compScalingFact) -- node[above] {false} (printSqueezedName);
            
            \node[switchnode, below=of compScalingFact] (truncPossible)
                {(Further) truncation \\ of the name possible?};
            \draw[truearrow] (compScalingFact) -- node[above, rotate=90] {true} (truncPossible);
            
            \node[cmdnode, right=of truncPossible] (radicalMethods)
                {\enquote{Radical Squeezing} or \enquote{Overfull \texttt{\textbackslash hbox}}};
            \draw[falsearrow] (truncPossible) -- node[above] {false} (radicalMethods);
            
            \node[cmdnode, below=of truncPossible] (applyTruncation)
                {Apply truncation; pass the truncated name to \sty{fitnames}' main routine};
            \draw[truearrow] (truncPossible) -- node[above, rotate=90] {true} (applyTruncation);
            
            \draw[->] (applyTruncation.west) -- ++(-1.65cm,0) |- node[above right, font={\itshape\small}] {RECURSION} (compose);
        \end{tikzpicture}
        
        \caption{\sty{fitnames}' approach for fitting names to an allowed width}
        \label{fig:alg-fittomaxwidth}
    \end{figure}
    
    
    % Paul-Otto Norbert Müller-Lüdenscheid
    \begin{figure}[h!b]\centering
        \begin{tikzpicture}[signal pointer angle=130]\sffamily\small
            % Left column: stacked signals
            \node[fill=yellow, below right, startsignal] (s0) {0};
            \foreach \nr/\abovenr/\color in {1/0/yellow!85!red, 2/1/yellow!70!red, 3/2/yellow!55!red}
                {\node[fill=\color, contsignal, above=.5ex of s\abovenr] (s\nr) {\nr};}
            \node[fill=yellow!40!red, endsignal, above=.5ex of s3]  (s4) {4};
            
            % Right column: background boxes
            \foreach \nde/\color in {s0/yellow, s1/yellow!85!red, s2/yellow!70!red, s3/yellow!55!red, s4/yellow!40!red}
                {\fill[fill=\color] ([xshift=.5ex]\nde.north east) rectangle ([xshift=11cm]\nde.south east);}
            
            % Right column: description
            \foreach \ndenr/\measure/\name in {%
                    0/{No truncation}/{Paul-Otto Norbert Müller-Lüdenscheid},
                    1/{Truncate first name}/{Paul-O. Müller-Lüdenscheid},
                    2/{Truncate last name}/{Paul-O. Müller-L.},
                    3/{Truncate first name to a single letter}/{P. Müller-L.},
                    4/{Omit first name}/{Müller-L.}}
                {\node[right=.75em, descrnode] (d\ndenr) at ($(s\ndenr.north east)!.5!(s\ndenr.south east)$)
                    {\textbf{\normalsize\measure} \hskip0pt plus 1fill\relax \textit{\name}};}
        \end{tikzpicture}

        \caption{Different levels of truncation applied to a (fictional) name}
        \label{fig:alg-truncation}
    \end{figure}
    
    
    
    
    
    
    % Start determining the checksum from here
    \StopEventually{%
        \clearpage
        \phantomsection
        \addcontentsline{toc}{part}{Index}%
        \PrintChanges
        \setcounter{IndexColumns}{2}
        \IndexPrologue{\section*{Index}}
        \PrintIndex}
    \clearpage
    
    
    
    
    \part{The package code}
    \CodelineNumbered
    \DocInput{fitnames.dtx}
    
    
    \Finale
\end{document}

